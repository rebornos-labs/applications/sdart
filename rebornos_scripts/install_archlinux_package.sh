#! /usr/bin/env sh

#┌─────────────────────────────────────────────────────────────────────────────┐
#│ SDaRT - System Diagnostics and Repair Tool                                  │
#│ ==========================================                                  │
#│ File   : sdart-rs/rebornos_scripts/install_archlinux_package.sh             │
#│ License: Mozilla Public License 2.0                                         │
#│ URL    : https://gitlab.com/shivanandvp/sdart-rs                            │
#│ Authors:                                                                    │
#│     1. shivanandvp <shivanandvp@rebornos.org>                               │
#│     2.                                                                      │
#│ -----                                                                       │
#│ Description:                                                                │
#│                                                                             │
#│                                                                             │
#│ -----                                                                       │
#│ Last Modified: Fri, 17th December 2021 8:19:03 AM                           │
#│ Modified By  :                                                              │
#│ -----                                                                       │
#│ 1. Copyright (c) 2021 shivanandvp <shivanandvp@rebornos.org>                │
#│ 2.                                                                          │
#└─────────────────────────────────────────────────────────────────────────────┘

SCRIPT_DIRECTORY="$(dirname -- "$(readlink -f -- "$0")")"
PROJECT_DIRECTORY="$(dirname -- "$SCRIPT_DIRECTORY")"

if ls "$SCRIPT_DIRECTORY"/archlinux_packaging/*.pkg.tar.* > /dev/null 2>&1;then
    set -o xtrace
    sudo pacman -U "$@" "$SCRIPT_DIRECTORY"/archlinux_packaging/*.pkg.tar.zst
    set +o xtrace
else
    set -o xtrace
    sh "$SCRIPT_DIRECTORY"/build_archlinux_package.sh --install "$@"
    set +o xtrace
fi