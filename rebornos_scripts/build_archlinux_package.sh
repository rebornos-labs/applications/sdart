#! /usr/bin/env sh

#┌─────────────────────────────────────────────────────────────────────────────┐
#│ SDaRT - System Diagnostics and Repair Tool                                  │
#│ ==========================================                                  │
#│ File   : sdart-rs/rebornos_scripts/build_archlinux_package.sh               │
#│ License: Mozilla Public License 2.0                                         │
#│ URL    : https://gitlab.com/shivanandvp/sdart-rs                            │
#│ Authors:                                                                    │
#│     1. shivanandvp <shivanandvp@rebornos.org>                               │
#│     2.                                                                      │
#│ -----                                                                       │
#│ Description:                                                                │
#│                                                                             │
#│                                                                             │
#│ -----                                                                       │
#│ Last Modified: Fri, 17th December 2021 8:18:38 AM                           │
#│ Modified By  :                                                              │
#│ -----                                                                       │
#│ 1. Copyright (c) 2021 shivanandvp <shivanandvp@rebornos.org>                │
#│ 2.                                                                          │
#└─────────────────────────────────────────────────────────────────────────────┘

SCRIPT_DIRECTORY="$(dirname -- "$(readlink -f -- "$0")")"
PROJECT_DIRECTORY="$(dirname -- "$SCRIPT_DIRECTORY")"

( # Create subshell to nullify directory changes on exit
    # Run makepkg
    set -o xtrace
    cd "$SCRIPT_DIRECTORY"/archlinux_packaging && \
    makepkg \
        --force \
        --syncdeps \
        "$@"
    set +o xtrace
)