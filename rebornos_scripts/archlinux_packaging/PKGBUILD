#! /usr/bin/env sh

#┌─────────────────────────────────────────────────────────────────────────────┐
#│ SDaRT - System Diagnostics and Repair Tool                                  │
#│ ==========================================                                  │
#│ File   : sdart-rs/rebornos_scripts/archlinux_packaging/PKGBUILD             │
#│ License: Mozilla Public License 2.0                                         │
#│ URL    : https://gitlab.com/shivanandvp/sdart-rs                            │
#│ Authors:                                                                    │
#│     1. shivanandvp <shivanandvp@rebornos.org>                               │
#│     2.                                                                      │
#│ -----                                                                       │
#│ Last Modified: Fri, 17th December 2021 8:21:55 AM                           │
#│ Modified By  : shivanandvp                                                  │
#│ -----                                                                       │
#│ 1. Copyright (c) 2021 shivanandvp <shivanandvp@rebornos.org>                │
#│ 2.                                                                          │
#└─────────────────────────────────────────────────────────────────────────────┘

# Maintainer: shivanandvp <shivanandvp@rebornos.org>
pkgname=sdart
pkgver=0.0.1
pkgrel=1
arch=('i686' 'x86_64' 'armv6h' 'armv7h')
pkgdesc="System Diagnostics and Repair Tool"
license=('MPL-2.0')
depends=('glib2'
         'gdk-pixbuf2'
         'gtk4'
         'graphene'
         'cairo'
         'pango'
         'xdg-utils'
         'polkit'
         'cantarell-fonts'
         'qrencode')
makedepends=('git'
             'rustup'
             'rust')
provides=("${pkgname}")
conflicts=("${pkgname}")
replaces=()
backup=()
options=()
install=
source=()
noextract=()
md5sums=()

instdir="/opt/SDaRT"

PROJECT_DIRECTORY="$(dirname -- "$(dirname -- "$(pwd)")")"
RESOURCE_DIRECTORY="$PROJECT_DIRECTORY/rebornos_scripts/archlinux_packaging"
NUMBER_OF_PROCESSORS="$(nproc)"

build() {
    # Starts in the `src` directory
    RUSTUP_TOOLCHAIN=nightly \
    cargo build \
        --release \
        --locked \
        --all-features \
        --target-dir="target"
}

check() {
    # Starts in the `src` directory
    RUSTUP_TOOLCHAIN=nightly \
    cargo test \
        --release \
        --locked \
        --all-features \
        --target-dir="target"
}

package() {
    # Starts in the `src` directory

    # Copy the media files
    install -d -m 755 "${pkgdir}/${instdir}/media" # Create the media directory
    cp -r "$RESOURCE_DIRECTORY"/media/* "${pkgdir}/${instdir}/media" # Copy the media files
    chmod 755 -R "${pkgdir}/${instdir}/media" # Assign correct permissions to the media files 

    # Copy the binaries
    install -d -m 755 "${pkgdir}/${instdir}/bin" # Temporary directory to store the binaries
    install -d -m 755 "${pkgdir}/usr/bin" # Create path to store the symbolic links to binaries 

    find target/release \
     -maxdepth 1 \
     -executable \
     -type f \
     \( ! -iname "*.d" -and ! -iname "*.rlib" -and ! -iname "*.cargo-lock" \) \
     -exec install -m 755 "{}" "${pkgdir}/${instdir}/bin" \; # Find executable binaries in the release directory and store it in the temporary bin directory so that they can be temporarily handled separately from other files in the application base directory

    for _executable in "${pkgdir}/${instdir}"/bin/*; do # For each collected binary file in the temporary bin directory
        _filename=$(basename "$_executable") # Get the filename from the path 
        mv "$_executable" "${pkgdir}/${instdir}/" # Move binary files back to the application directory
        ln -s "${instdir}/${_filename}" "${pkgdir}/usr/bin/${_filename}" # Created symbolic links to the application binaries in the /usr/bin directory
    done      
    rmdir "${pkgdir}/${instdir}/bin" # Remove the temporary binary directory    

    # Copy the .desktop entry to the appropriate location with the correct permissions
    install -D -m 644 "$RESOURCE_DIRECTORY/${pkgname}.desktop" "${pkgdir}/usr/share/applications/${pkgname}.desktop"

    # Copy the License and Readme files
    (
        cd "$PROJECT_DIRECTORY" && \
        install -m 644 LICENSE README.md CHANGELOG "${pkgdir}/${instdir}/" 
    )  
}