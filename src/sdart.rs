//┌────────────────────────────────────────────────────────────────────────────┐
//│ SDaRT - System Diagnostics and Repair Tool                                 │
//│ ==========================================                                 │
//│ File   : sdart-rs/src/sdart.rs                                             │
//│ License: Mozilla Public License 2.0                                        │
//│ URL    : https://gitlab.com/shivanandvp/sdart-rs                           │
//│ Authors:                                                                   │
//│     1. shivanandvp <shivanandvp@rebornos.org>                              │
//│     2.                                                                     │
//│ -----                                                                      │
//│ Description:                                                               │
//│                                                                            │
//│                                                                            │
//│ -----                                                                      │
//│ Last Modified: Sun, 5th December 2021 5:16:20 PM                           │
//│ Modified By  :                                                             │
//│ -----                                                                      │
//│ 1. Copyright (c) 2021 shivanandvp <shivanandvp@rebornos.org>               │
//│ 2.                                                                         │
//└────────────────────────────────────────────────────────────────────────────┘

use clap::{App, load_yaml};
use sdart::diagnostics::*;

mod commandline {
    use clap::ArgMatches;
    use sdart::diagnostics::*;

    pub mod handler {
        use clap::ArgMatches;
        use sdart::diagnostics::*;
        
        pub fn logs(matches: &ArgMatches) -> Result<(), Error>{
            let supported_log_list = logs::LogList::new()?;
            if let Some(log_source) = matches.value_of("log_source") {
                let log_text = supported_log_list.log_text(log_source)?;
                println!("{}", log_text);
            }

            Ok(())
        }
    }
    
    pub fn process_options(matches: &ArgMatches) -> Result<(), Error>{
        if let Some(matches) = matches.subcommand_matches("logs") {
            handler::logs(matches)?;
        }

        Ok(())
    }
}

fn main() {
    let yaml = load_yaml!("sdart_cli.yaml");
    let matches = App::from(yaml).get_matches();     
    if let Err(error) = commandline::process_options(&matches) {
        eprintln!("Error: {}", error);
        std::process::exit(1);
    }
}
