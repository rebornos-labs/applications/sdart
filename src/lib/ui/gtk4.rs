//┌────────────────────────────────────────────────────────────────────────────┐
//│ SDaRT - System Diagnostics and Repair Tool                                 │
//│ ==========================================                                 │
//│ File   : sdart-rs/src/lib/ui/gtk4.rs                                       │
//│ License: Mozilla Public License 2.0                                        │
//│ URL    : https://gitlab.com/shivanandvp/sdart-rs                           │
//│ Authors:                                                                   │
//│     1. shivanandvp <shivanandvp@rebornos.org>                              │
//│     2.                                                                     │
//│ -----                                                                      │
//│ Description:                                                               │
//│                                                                            │
//│                                                                            │
//│ -----                                                                      │
//│ Last Modified: Sat, 4th December 2021 2:54:29 PM                           │
//│ Modified By  :                                                             │
//│ -----                                                                      │
//│ 1. Copyright (c) 2021 shivanandvp <shivanandvp@rebornos.org>               │
//│ 2.                                                                         │
//└────────────────────────────────────────────────────────────────────────────┘

use gtk4 as gtk;
use gtk4::prelude::*;

const WINDOW_HEIGHT: i32 = 350;
const WINDOW_ASPECT_RATIO: f32 = 1.61803398875;

pub fn main_window(application: &gtk::Application) {
    let window = gtk::ApplicationWindow::new(application);
    window.set_title(Some("SDaRT - System Diagnostics and Repair Tool"));
    window.set_default_size((WINDOW_HEIGHT as f32 * WINDOW_ASPECT_RATIO) as i32, WINDOW_HEIGHT);
    window.set_resizable(true);    
    window.show();

    // let builder = gtk::Builder::from_string(include_str!("sdart-gui.ui"));
    // let window: gtk::ApplicationWindow = builder
    //     .object("main_window")
    //     .expect("Could not get object `main_window` from builder.");
    // window.set_application(Some(application));
    //  window.present();
}

pub fn load() {
    let application = gtk::Application::new(Some("org.shivanandvp.sdart"), Default::default());
    application.connect_activate(main_window);
    application.run();
}