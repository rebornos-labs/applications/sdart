//┌────────────────────────────────────────────────────────────────────────────┐
//│ SDaRT - System Diagnostics and Repair Tool                                 │
//│ ==========================================                                 │
//│ File   : sdart-rs/src/lib/paste/privatebin.rs                              │
//│ License: Mozilla Public License 2.0                                        │
//│ URL    : https://gitlab.com/shivanandvp/sdart-rs                           │
//│ Authors:                                                                   │
//│     1. shivanandvp <shivanandvp@rebornos.org>                              │
//│     2.                                                                     │
//│ -----                                                                      │
//│ Description:                                                               │
//│                                                                            │
//│                                                                            │
//│ -----                                                                      │
//│ Last Modified: Mon, 6th December 2021 1:41:35 AM                           │
//│ Modified By  :                                                             │
//│ -----                                                                      │
//│ 1. Copyright (c) 2021 shivanandvp <shivanandvp@rebornos.org>               │
//│ 2.                                                                         │
//└────────────────────────────────────────────────────────────────────────────┘

// region: API FACADE and IMPORTS

// Privately imports for use in this module
use crate::paste::*;
use aes_gcm::aes::Aes256;
use ring::pbkdf2;
use snafu::ResultExt;
use url::Url;

// Publicly re-exports necessary items for use from outside this module
pub use data_model::*;
pub use encryption::*;

// endregion: API FACADE and IMPORTS

mod data_model {

    // region: API FACADE and IMPORTS

    // Privately imports for use in this module

    use serde::{Deserialize, Serialize};
    use crate::paste::*;
    use snafu::ResultExt;

    // endregion: API FACADE and IMPORTS

    // region: RAW REQUEST DATA
    // Deserialized forms of JSON Request Data for the web API of PrivateBin

    /// Deserialized form of the JSON paste data that is sent to the web API of PrivateBin
    /// The JSON data takes the below form:
    /// ```js
    /// {
    ///     "v": 2, // Version (recommended: 2)
    ///     "adata": [ // JSON associated data that is used in the paste encryption to authenticate any sent copies of the associated data
    ///         [
    ///             string, // Base64 encoded Cipher Initialization Vector (IV)
    ///             string, // Base64 encoded Key Derivation Function (KDF) salt
    ///             number, // KDF iterations (recommended: 100000)
    ///             number, // KDF keysize (recommended: 256)
    ///             number, // Cipher tag size in bits (recommended: 128)
    ///             string, // Cipher algorithm (recommended: "aes")
    ///             string, // Cipher mode (recommended: "gcm")
    ///             string, // Compression type (recommended: "zlib")
    ///         ],
    ///         string, // paste format: One of "plaintext","syntaxhighlighting", or "markdown",
    ///         number, // open-discussion flag: Either 0 or 1
    ///         number, // burn-after-reading flag: Either 0 or 1
    ///     ],
    ///     "ct": string, // Base64 encoded Cipher Text
    ///     "meta": {
    ///         "expire": string // Expire after duration (examples: "5min", "1day", "2year", and "never")
    ///     }
    /// }
    /// ```
    /// [Source] (https://github.com/PrivateBin/PrivateBin/wiki/API#as-of-version-13)
    #[derive(Serialize, Deserialize, Debug)]
    pub struct PasteRequestData {
        pub v: u32,
        pub adata: PasteAdata,
        pub ct: String,
        pub meta: PasteMeta,
    }

    /// Deserialized form of the JSON associated data that is used in the paste encryption to authenticate any sent copies of the associated data
    /// The JSON data takes the below form:
    /// ```js
    /// {
    ///     [
    ///         string, // Base64 encoded Cipher Initialization Vector (IV)
    ///         string, // Base64 encoded Key Derivation Function (KDF) salt
    ///         number, // KDF iterations (recommended: 100000)
    ///         number, // KDF keysize (recommended: 256)
    ///         number, // Cipher tag size in bits (recommended: 128)
    ///         string, // cipher algorithm (recommended: "aes")
    ///         string, // cipher mode (recommended: "gcm")
    ///         string, // compression type (recommended: "zlib")
    ///     ],
    /// }
    /// ```
    /// [Source] (https://github.com/PrivateBin/PrivateBin/wiki/API#as-of-version-13)
    pub type PasteAdata = (
        (String, String, u32, u32, u32, String, String, String),
        String,
        u8,
        u8,
    );

    /// Deserialized form of the JSON comment data that is sent to the web API of PrivateBin
    /// The JSON data takes the below form:
    /// ```js
    /// {
    ///     "v": 2, // Version (recommended: 2)
    ///     "adata": [ // JSON associated data that is used in the paste encryption to authenticate any sent copies of the associated data
    ///         string, // Base64 encoded Cipher Initialization Vector (IV)
    ///         string, // Base64 encoded Key Derivation Function (KDF) salt
    ///         number, // KDF iterations (recommended: 100000)
    ///         number, // KDF keysize (recommended: 256)
    ///         number, // Cipher tag size in bits (recommended: 128)
    ///         string, // Cipher algorithm (recommended: "aes")
    ///         string, // Cipher mode (recommended: "gcm")
    ///         string, // Compression type (recommended: "zlib")
    ///     ],
    ///     "ct": string, // Base64 encoded Cipher Text,
    ///     "pasteid": string, // ID of the paste to comment on
    ///     "parentid": string, // ID of the paste or comment to comment on
    /// }
    /// ```
    /// [Source] (https://github.com/PrivateBin/PrivateBin/wiki/API#as-of-version-13)
    #[derive(Serialize, Deserialize, Debug)]
    pub struct CommentRequestData {
        pub v: u32,
        pub adata: CommentAdata,
        pub ct: String,
        pub pasteid: String, 
        pub parentid: String, 
    }

    /// Deserialized form of the JSON associated data that is used in the comment encryption to authenticate any sent copies of the associated data
    /// The JSON data takes the below form:
    /// ```js
    /// {
    ///     [
    ///         string, // Base64 encoded Cipher Initialization Vector (IV)
    ///         string, // Base64 encoded Key Derivation Function (KDF) salt
    ///         number, // KDF iterations (recommended: 100000)
    ///         number, // KDF keysize (recommended: 256)
    ///         number, // Cipher tag size in bits (recommended: 128)
    ///         string, // cipher algorithm (recommended: "aes")
    ///         string, // cipher mode (recommended: "gcm")
    ///         string, // compression type (recommended: "zlib")
    ///     ],
    /// }
    /// ```
    /// [Source] (https://github.com/PrivateBin/PrivateBin/wiki/API#as-of-version-13)
    pub type CommentAdata = (String, String, u32, u32, u32, String, String, String);

    /// Deserialized form of the paste contents that are going to be compressed and then encrypted into cipher data before sending to a PrivateBin instance
    /// The JSON data takes the below form:
    /// {
    ///     "paste": string, // The textual content of the paste
    ///     "attachment": string, // [data URI as per RFC 2397]
    ///     "attachment_name": // filename,
    ///     "children": [
    ///         string, // URL, for example "paste_id#key"
    ///         string, // More URLs, for example "https://example.com/"
    ///         ...
    ///         ...
    ///     ]
    /// }
    /// [Source] (https://github.com/PrivateBin/PrivateBin/wiki/Encryption-format#data-passed-in)
    #[derive(Serialize, Deserialize, Debug)]
    pub struct PasteContentData {
        pub paste: String,
        #[serde(skip_serializing_if = "Option::is_none")]
        pub attachment: Option<String>,
        #[serde(skip_serializing_if = "Option::is_none")]
        pub attachment_name: Option<String>,
        #[serde(skip_serializing_if = "Option::is_none")]
        pub children: Option<Vec<String>>,
    }

    /// Deserialized form of the comment contents that are going to be compressed and then encrypted into cipher data before sending to a PrivateBin instance
    /// The JSON data takes the below form:
    /// {
    ///     "comment": string, // The textual content of the comment
    /// }
    /// [Source] (https://github.com/PrivateBin/PrivateBin/wiki/Encryption-format#data-passed-in)
    #[derive(Serialize, Deserialize, Debug)]
    pub struct CommentContentData {
        pub comment: String,
        pub nickname: String,
    }

    /// The paste metadata
    #[derive(Serialize, Deserialize, Debug)]
    pub struct PasteMeta {
        /// Expiry duration indicated by either 'never', or a number followed by 'min', 'hour', 'day', 'week', 'month' or 'year'
        pub expire: String,
    }

    /// Deserialized form of the JSON delete data that is sent to the web API of PrivateBin
    /// The JSON data takes the below form:
    /// ```js
    /// {
    ///     "pasteid": string,
    ///     "deletetoken": string
    /// }
    /// ```
    /// [Source] (https://github.com/PrivateBin/PrivateBin/wiki/API#as-of-version-13)
    #[derive(Serialize, Deserialize, Debug)]
    pub struct DeleteRequestData {
        pub pasteid: String,
        pub deletetoken: String,
    }

    // endregion: REQUEST DATA

    // region: RAW RESPONSE DATA
    // Deserialized forms of JSON Response Data for the web API of PrivateBin

    /// Deserialized form of the JSON paste response data that is sent by the web API of PrivateBin
    /// [Source] (https://github.com/PrivateBin/PrivateBin/wiki/API#as-of-version-13)
    #[derive(Clone, Serialize, Deserialize, Debug)]
    pub struct ResponseData {
        pub status: u32,
        pub id: Option<String>,
        pub url: Option<String>,
        pub deletetoken: Option<String>,
        pub message: Option<String>,
    }

    // endregion: RAW RESPONSE DATA

    // region: ERGONOMIC DATA

    /// The associated data in [PasteAdata] stored in a more readable form
    #[derive(Debug)]
    pub struct PasteAssociatedData<const IVN: usize, const SALTN: usize> {
        /// Cipher Initialization Vector (IV)
        cipher_initial_vector: [u8; IVN],
        /// Key Derivation Function (KDF) salt
        key_salt: [u8; SALTN],
        /// Key Derivation Function (KDF) iterations
        key_iterations: u32,
        /// The size of the random part of the KDF key in bits (the other part is user-provided)
        key_size: u32,
        /// The tag size of the Cipher in bits
        cipher_tag_size: u32,
        cipher_algorithm: CipherAlgorithm,
        cipher_mode: CipherMode,
        compression_type: CompressionType,
        paste_display_format: PasteDisplayFormat,
        open_discussion_flag: bool,
        burn_after_reading: bool,
    }

    /// The associated data in [CommentAdata] stored in a more readable form
    #[derive(Debug)]
    pub struct CommentAssociatedData<const IVN: usize, const SALTN: usize> {
        /// Cipher Initialization Vector (IV)
        cipher_initial_vector: [u8; IVN],
        /// Key Derivation Function (KDF) salt
        key_salt: [u8; SALTN],
        /// Key Derivation Function (KDF) iterations
        key_iterations: u32,
        /// The size of the random part of the KDF key in bits (the other part is user-provided)
        key_size: u32,
        /// The tag size of the Cipher in bits
        cipher_tag_size: u32,
        cipher_algorithm: CipherAlgorithm,
        cipher_mode: CipherMode,
        compression_type: CompressionType,
    }

    // endregion: ERGONOMIC DATA

    // region: ERGONOMIC TO RAW DATA

    impl<const IVN: usize, const SALTN: usize> From<PasteAssociatedData<IVN, SALTN>> for PasteAdata {
        fn from(paste_associated_data: PasteAssociatedData<IVN, SALTN>) -> Self {
            return (
                (
                    base64::encode(paste_associated_data.cipher_initial_vector),
                    base64::encode(paste_associated_data.key_salt),
                    paste_associated_data.key_iterations,
                    paste_associated_data.key_size,
                    paste_associated_data.cipher_tag_size,
                    paste_associated_data.cipher_algorithm.into(),
                    paste_associated_data.cipher_mode.into(),
                    paste_associated_data.compression_type.into(),
                ),
                paste_associated_data.paste_display_format.into(),
                match paste_associated_data.open_discussion_flag {
                    true => 1u8,
                    false => 0u8,
                }, /* open-discussion flag - 1 or 0 */
                match paste_associated_data.burn_after_reading {
                    true => 1u8,
                    false => 0u8,
                }, /* burn-after-reading flag - 1 or 0 */
            );
        }
    }

    impl<const IVN: usize, const SALTN: usize> From<CommentAssociatedData<IVN, SALTN>>
        for CommentAdata
    {
        fn from(comment_associated_data: CommentAssociatedData<IVN, SALTN>) -> Self {
            return (
                base64::encode(comment_associated_data.cipher_initial_vector),
                base64::encode(comment_associated_data.key_salt),
                comment_associated_data.key_iterations,
                comment_associated_data.key_size,
                comment_associated_data.cipher_tag_size,
                comment_associated_data.cipher_algorithm.into(),
                comment_associated_data.cipher_mode.into(),
                comment_associated_data.compression_type.into(),
            );
        }
    }

    // endregion: ERGONOMIC TO RAW DATA

    // region: String TO RAW DATA

    impl TryFrom<String> for PasteMeta {
        type Error = Error;

        /// Express duration provided as natural language text to approximate (rounded off) duration in units that PrivateBin understands
        /// Example of duration that can be specified: "7 days 15 hours 3 min"
        fn try_from(expiry_string: String) -> Result<Self, Self::Error> {
            use chrono::naive::NaiveDateTime;
            use chronoutil::relative_duration::RelativeDuration;
            use regex::RegexBuilder;
            use snafu::ensure;
            use std::time::SystemTime;

            let regex_pattern = r#"(?x)
                            (?:
                                (?P<count>
                                    [0-9]{1,3}
                                    (?:,[0-9]{3})*
                                    (?:\.[0-9]+)?|\.[0-9]+
                                )
                                \s*
                                (?:
                                    (?:(?P<seconds>(?:second|sec|s))s?)
                                    | (?:(?P<minutes>(?:minute|min|m))s?)
                                    | (?:(?P<hours>(?:hour|hr|h))s?)
                                    | (?:(?P<days>(?:day|dy|d))s?)
                                    | (?:(?P<weeks>(?:week|wk|w))s?)
                                    | (?:(?P<months>(?:month|mon|mo))s?)
                                    | (?:(?P<years>(?:year|yr|y))s?)
                                )
                            ) | (?P<never>never|infinity|inf|forever)
                        "#;
            let expiry_regex = RegexBuilder::new(regex_pattern)
                .case_insensitive(true)
                .ignore_whitespace(true)
                .build()
                .context(super::DurationRegexError { regex_pattern })?;
            let current_instant = NaiveDateTime::from_timestamp(
                SystemTime::now()
                    .duration_since(SystemTime::UNIX_EPOCH)
                    .context(crate::paste::privatebin::PastSystemTimeError {})?
                    .as_secs()
                    .try_into()
                    .context(crate::paste::privatebin::FutureSystemTimeError {})?,
                0,
            );
            let mut expiry_datetime =
                NaiveDateTime::new(current_instant.date(), current_instant.time());
            for regex_match in expiry_regex.captures_iter(&expiry_string) {
                if regex_match.name("seconds").is_some() {
                    expiry_datetime = expiry_datetime
                        + RelativeDuration::seconds(regex_match["count"].parse::<i64>().unwrap());
                } else if regex_match.name("minutes").is_some() {
                    expiry_datetime = expiry_datetime
                        + RelativeDuration::minutes(regex_match["count"].parse::<i64>().unwrap());
                } else if regex_match.name("hours").is_some() {
                    expiry_datetime = expiry_datetime
                        + RelativeDuration::hours(regex_match["count"].parse::<i64>().unwrap());
                } else if regex_match.name("days").is_some() {
                    expiry_datetime = expiry_datetime
                        + RelativeDuration::days(regex_match["count"].parse::<i64>().unwrap());
                } else if regex_match.name("weeks").is_some() {
                    expiry_datetime = expiry_datetime
                        + RelativeDuration::weeks(regex_match["count"].parse::<i64>().unwrap());
                } else if regex_match.name("months").is_some() {
                    expiry_datetime = expiry_datetime
                        + RelativeDuration::months(regex_match["count"].parse::<i32>().unwrap());
                } else if regex_match.name("years").is_some() {
                    expiry_datetime = expiry_datetime
                        + RelativeDuration::years(regex_match["count"].parse::<i32>().unwrap());
                } else if regex_match.name("never").is_some() {
                    return Ok(PasteMeta {
                        expire: String::from("never"),
                    });
                }
            }
            let duration = expiry_datetime.signed_duration_since(current_instant);
            let duration_in_minutes: i64 = duration.num_minutes();
            ensure!(
                duration_in_minutes > 0,
                crate::paste::privatebin::ExpiryFormatError {
                    duration_in_seconds: duration.num_seconds()
                }
            );
            let mut expiry_string;
            if duration_in_minutes < 60 * 5 {
                // If duration is less than 5 hours, express time in minutes
                expiry_string = duration.num_minutes().to_string();
                expiry_string.push_str("min");
            } else if duration_in_minutes < 60 * 24 * 5 {
                // If duration is less than 5 days, express time in hours
                expiry_string = duration.num_hours().to_string();
                expiry_string.push_str("hours");
            } else if duration_in_minutes < 60 * 24 * 7 * 5 {
                // If duration is less than 5 weeks, express time in days
                expiry_string = duration.num_days().to_string();
                expiry_string.push_str("days");
            } else if duration_in_minutes < 60 * 24 * 30 * 5 {
                // If duration is less than 150 days, express time in weeks
                expiry_string = duration.num_weeks().to_string();
                expiry_string.push_str("weeks");
            } else if duration_in_minutes < 60 * 24 * 7 * 52 * 5 {
                // If duration is less than 260 weeks, express time in weeks
                expiry_string = duration.num_weeks().to_string();
                expiry_string.push_str("weeks");
            } else {
                expiry_string = duration.num_weeks().to_string();
                expiry_string.push_str("weeks");
            }

            return Ok(PasteMeta {
                expire: expiry_string,
            });
        }
    }

    // endregion: String TO RAW DATA

    // region: Default IMPLEMENTATIONS

    impl<const IVN: usize, const SALTN: usize> Default for PasteAssociatedData<IVN, SALTN> {
        fn default() -> Self {
            return PasteAssociatedData {
                cipher_initial_vector: [0u8; IVN],
                key_salt: [0u8; SALTN],
                key_iterations: 100000,
                key_size: 256,
                cipher_tag_size: 128,
                cipher_algorithm: CipherAlgorithm::Aes,
                cipher_mode: CipherMode::Gcm,
                compression_type: CompressionType::Zlib,
                paste_display_format: PasteDisplayFormat::PlainText,
                open_discussion_flag: false, // TODO: change this to `true`
                burn_after_reading: false,
            };
        }
    }

    impl<const IVN: usize, const SALTN: usize> Default for CommentAssociatedData<IVN, SALTN> {
        fn default() -> Self {
            return CommentAssociatedData {
                cipher_initial_vector: [0u8; IVN],
                key_salt: [0u8; SALTN],
                key_iterations: 100000,
                key_size: 256,
                cipher_tag_size: 128,
                cipher_algorithm: CipherAlgorithm::Aes,
                cipher_mode: CipherMode::Gcm,
                compression_type: CompressionType::Zlib,
            };
        }
    }

    // endregion: Default IMPLEMENTATIONS

    // region: CONSTRUCTORS

    impl<const IVN: usize, const SALTN: usize> PasteAssociatedData<IVN, SALTN> {
        pub fn from_inputs<const PASSN: usize>(
            encryption_input: &super::encryption::EncryptionInput<PASSN, SALTN, IVN>,
            paste_input_interface: &mut PasteInputInterface,
        ) -> Self {
            PasteAssociatedData {
                cipher_initial_vector: encryption_input.cipher_initial_vector,
                key_salt: encryption_input.key_salt,
                key_iterations: encryption_input.key_iterations,
                cipher_algorithm: paste_input_interface.cipher_algorithm,
                cipher_mode: paste_input_interface.cipher_mode,
                compression_type: paste_input_interface.compression_type,
                paste_display_format: paste_input_interface.paste_display_format,
                open_discussion_flag: paste_input_interface.open_discussion,
                burn_after_reading: paste_input_interface.burn_after_reading,
                ..Default::default()
            }
        }
    }

    impl<const IVN: usize, const SALTN: usize> CommentAssociatedData<IVN, SALTN> {
        pub fn from_inputs<const PASSN: usize>(
            encryption_input: &super::encryption::EncryptionInput<PASSN, SALTN, IVN>,
            comment_input_interface: &CommentInputInterface,
        ) -> Self {
            CommentAssociatedData {
                cipher_initial_vector: encryption_input.cipher_initial_vector,
                key_salt: encryption_input.key_salt,
                key_iterations: encryption_input.key_iterations,
                cipher_algorithm: comment_input_interface.cipher_algorithm,
                cipher_mode: comment_input_interface.cipher_mode,
                compression_type: comment_input_interface.compression_type,
                ..Default::default()
            }
        }
    }

    // endregion: CONSTRUCTORS
}

mod encryption {

    // region: API FACADE and IMPORTS

    // Privately imports for use in this module

    use aes_gcm::{
        aead::{AeadInPlace, NewAead},
        aes::{Aes256, BlockCipher, BlockEncrypt},
    };
    use cipher::NewBlockCipher;
    use ring::{pbkdf2, rand, rand::SecureRandom};
    use std::num::NonZeroU32;

    // endregion: API FACADE and IMPORTS

    // region: DATA

    #[derive(Debug)]
    pub struct EncryptionInput<
        const PASSN: usize, // Size (in bytes) of the key password. PASSN is arbitrary
        const SALTN: usize, // Size (in bytes) of the key salt. SALTN is arbitrary
        const IVN: usize, // Size (in bytes) of the cipher initialization vector. IVN is arbitrary
    > {
        pub given_password: String,
        pub key_password: [u8; PASSN],
        pub key_salt: [u8; SALTN],
        pub key_iterations: u32,
        pub cipher_initial_vector: [u8; IVN],
    }

    // endregion: DATA

    // region: CONSTRUCTORS

    impl<const PASSN: usize, const SALTN: usize, const IVN: usize> EncryptionInput<PASSN, SALTN, IVN> {
        pub fn new() -> Self {
            return EncryptionInput {
                given_password: "".to_string(),
                key_password: [
                            0u8; // Zero as an unsigned 8 bit integer (i.e. stored in a byte)
                            PASSN // PASSN bytes or PASSN*8 bits. 
                        ],
                key_salt: [
                            0u8; // Zero as an unsigned 8 bit integer (i.e. stored in a byte)
                            SALTN // SALTN bytes or SALTN*8 bits
                        ],
                key_iterations: 100000,
                cipher_initial_vector: [
                            0u8; // Zero as an unsigned 8 bit integer (i.e. stored in a byte)
                            IVN // IVN bytes or IVN*8 bits
                        ],
            };
        }

        pub fn from_random() -> Self {
            let mut encryption_input = EncryptionInput::new();

            let random_number_generator = rand::SystemRandom::new();
            random_number_generator
                .fill(&mut encryption_input.key_password)
                .expect("ERROR: Unable to generate random passphrase for key_password...");
            random_number_generator
                .fill(&mut encryption_input.key_salt)
                .expect("ERROR: Unable to generate random passphrase for key_salt...");
            random_number_generator
                .fill(&mut encryption_input.cipher_initial_vector)
                .expect("ERROR: Unable to generate random passphrase for cipher_initial_vector...");

            return encryption_input;
        }
    }

    // endregion: CONSTRUCTORS

    // region: CRYPTOGRAPHIC KEY

    pub fn derive_pbkdf2_key<
        const PASSN: usize, // Size (in bytes) of the key password
        const SALTN: usize, // Size (in bytes) of the key salt
        const KEYN: usize, // Size (in bytes) of the generated cryptographic key. KEYN is arbitrary
        const IVN: usize,  // Size (in bytes) of the cipher initialization vector
    >(
        key_algorithm: ring::pbkdf2::Algorithm, /* Examples: ring::pbkdf2::{PBKDF2_HMAC_SHA1 or PBKDF2_HMAC_SHA256 or PBKDF2_HMAC_SHA384 or PBKDF2_HMAC_SHA512} */
        encryption_input: &EncryptionInput<PASSN, SALTN, IVN>,
    ) -> [u8; KEYN] {
        let mut pbkdf2_key = [
                0u8; // Zero as an unsigned 8 bit integer (i.e. stored in a byte)
                KEYN // 32 bytes or 256 bits
            ];

        let pbkdf2_key_password = [
            &encryption_input.key_password,
            encryption_input.given_password.as_bytes(),
        ]
        .concat();

        pbkdf2::derive(
            key_algorithm,
            NonZeroU32::new(encryption_input.key_iterations)
                .expect("Could not convert iterations to NonZeroU32..."),
            &encryption_input.key_salt,
            &pbkdf2_key_password,
            &mut pbkdf2_key,
        );

        return pbkdf2_key;
    }

    // endregion: CRYPTOGRAPHIC KEY

    // region: ENCRYPTION

    pub fn encrypt_aes_gcm<
        Aes, /* aes_gcm::aes::{Aes128 or Aes128Ctr or Aes192 or Aes192Ctr or Aes256 or Aes256Ctr} */
        const PASSN: usize, // Size (in bytes) of the key password
        const SALTN: usize, // Size (in bytes) of the key salt
        const KEYN: usize, // Size (in bytes) of the generated cryptographic key. KEYN is arbitrary
        const IVN: usize, // Size (in bytes) of the cipher initialization vector
    >(
        plain_data: Vec<u8>,    // Data to be encrypted, as a byte vector
        associated_data: &[u8], // The associated data, as a reference to a byte vector
        key: &[u8; KEYN],       // The encryption key
        encryption_input: &EncryptionInput<PASSN, SALTN, IVN>,
    ) -> Vec<u8>
    where
        Aes: NewBlockCipher + BlockCipher<BlockSize = generic_array::typenum::U16> + BlockEncrypt,
        Aes::ParBlocks: generic_array::ArrayLength<cipher::Block<Aes>>,
    {
        let mut buffer = plain_data;
        let algorithm =
            aes_gcm::AesGcm::<Aes256, generic_array::typenum::U16>::new_from_slice(key) // TODO: Replace U16 with a conversion from the generic IVN, and Aes256 with the generic Aes
                .expect("Unable to initialize the AES_GCM algorithm with the key...");
        let cipher_initial_vector =
            generic_array::GenericArray::<u8, generic_array::typenum::U16>::clone_from_slice(
                &encryption_input.cipher_initial_vector,
            ); // TODO: Replace U16 with a conversion from the generic IVN

        algorithm
            .encrypt_in_place(&cipher_initial_vector, &associated_data, &mut buffer)
            .expect("Error creating cipher text using the AES_GCM algorithm...");

        return buffer;
    }

    // endregion: ENCRYPTION
}

// region: PRIVATE FUNCTIONS

fn generate_paste_request_string<
    const PASSN: usize,
    const SALTN: usize,
    const KEYN: usize,
    const IVN: usize,
>(
    encryption_input: encryption::EncryptionInput<PASSN, SALTN, IVN>,
    paste_input_interface: &mut PasteInputInterface,
) -> Result<(String, String), error::Error> {
    // Derive a PBKDF2 key
    let pbkdf2_key = encryption::derive_pbkdf2_key(pbkdf2::PBKDF2_HMAC_SHA256, &encryption_input);

    // Store the paste text into the format accepted by PrivateBin
    let paste_contents = PasteContentData {
        paste: paste_input_interface.paste_text.clone(),
        attachment: None,
        attachment_name: None,
        children: None,
    };
    let paste_contents = serde_json::to_string(&paste_contents).context(JsonSerializeError {
        given_data: format!("{:?}", &paste_contents),
    })?;

    // Compress the text if needed
    let compressed_text = match paste_input_interface.compression_type {
        CompressionType::Zlib => {
            deflate::deflate_bytes_conf(
                paste_contents.as_bytes().into(),
                deflate::Compression::Best,
            )
        }
        CompressionType::None => paste_contents.as_bytes().into(),
    };

    let associated_data =
        PasteAssociatedData::from_inputs(&encryption_input, paste_input_interface);
    let associated_data: PasteAdata = associated_data.into();
    let associated_data_bytes =
        serde_json::to_string(&associated_data).context(JsonSerializeError {
            given_data: format!("{:?}", &associated_data),
        })?;
    let associated_data_bytes = associated_data_bytes.as_bytes();

    // Encrypt the paste
    let cipher_text = encryption::encrypt_aes_gcm::<Aes256, PASSN, SALTN, KEYN, IVN>(
        compressed_text,
        associated_data_bytes,
        &pbkdf2_key,
        &encryption_input,
    );

    // Fill the paste request model
    let paste_request_data = PasteRequestData {
        v: 2, // version (recommended 2)
        adata: associated_data,
        ct: base64::encode(cipher_text), // base64 encoded cipher text
        meta: PasteMeta::try_from(paste_input_interface.expiry_duration.clone())?,
    };

    let decryption_key = bs58::encode(encryption_input.key_password).into_string();

    let paste_request_data =
        serde_json::to_string(&paste_request_data).context(JsonSerializeError {
            given_data: format!("{:?}", &paste_request_data),
        })?;

    Ok((paste_request_data, decryption_key))
}

fn generate_comment_request_string<
    const PASSN: usize,
    const SALTN: usize,
    const KEYN: usize,
    const IVN: usize,
>(
    encryption_input: encryption::EncryptionInput<PASSN, SALTN, IVN>,
    comment_input_interface: &CommentInputInterface,
) -> Result<(String, String), Error> {  
    // Derive a PBKDF2 key
    let pbkdf2_key = encryption::derive_pbkdf2_key(pbkdf2::PBKDF2_HMAC_SHA256, &encryption_input);

    // Store the paste text into the format accepted by PrivateBin
    let comment_contents = CommentContentData {
        comment: comment_input_interface.comment_text.clone(),
        nickname: comment_input_interface.nickname.clone(),
    };
    let comment_contents = serde_json::to_string(&comment_contents).context(JsonSerializeError {
        given_data: format!("{:?}", &comment_contents),
    })?;
    println!("comment_contents_json: {}", comment_contents);

    // Compress the text if needed
    let compressed_text = match comment_input_interface.compression_type {
        CompressionType::Zlib => {
            deflate::deflate_bytes_conf(
                comment_contents.as_bytes().into(),
                deflate::Compression::Best,
            )
        }
        CompressionType::None => comment_contents.as_bytes().into(),
    };

    let associated_data =
        CommentAssociatedData::from_inputs(&encryption_input, comment_input_interface);
    let associated_data: CommentAdata = associated_data.into();
    let associated_data_bytes =
        serde_json::to_string(&associated_data).context(JsonSerializeError {
            given_data: format!("{:?}", &associated_data),
        })?;
    let associated_data_bytes = associated_data_bytes.as_bytes();

    // Encrypt the paste
    let cipher_text = encryption::encrypt_aes_gcm::<Aes256, PASSN, SALTN, KEYN, IVN>(
        compressed_text,
        associated_data_bytes,
        &pbkdf2_key,
        &encryption_input,
    );

    // Fill the paste request model
    let comment_request_data = CommentRequestData {
        v: 2, // version (recommended 2)
        adata: associated_data,
        ct: base64::encode(cipher_text), // base64 encoded cipher text
        pasteid: comment_input_interface.paste_id.clone(),
        parentid: comment_input_interface.parent_id.clone(),
    };

    let decryption_key = bs58::encode(encryption_input.key_password).into_string();

    let paste_request_data =
        serde_json::to_string(&comment_request_data).context(JsonSerializeError {
            given_data: format!("{:?}", &comment_request_data),
        })?;

    Ok((paste_request_data, decryption_key))
}

fn paste_request_body(
    paste_input_interface: &mut PasteInputInterface,
) -> Result<(String, String), Error> {
    // Prepare starting values for encryption
    let mut encryption_input =
        encryption::EncryptionInput::<32usize, 8usize, 16usize>::from_random();
    encryption_input.given_password = paste_input_interface.password.clone();

    Ok(generate_paste_request_string::<
        32usize,
        8usize,
        32usize,
        16usize,
    >(encryption_input, paste_input_interface))?
}

fn comment_request_body(
    comment_input_interface: &mut CommentInputInterface,
) -> Result<(String, String), Error> {
    // Prepare starting values for encryption
    let mut encryption_input =
        encryption::EncryptionInput::<32usize, 8usize, 16usize>::from_random();
    encryption_input.given_password = comment_input_interface.password.clone();
    encryption_input.key_password = bs58::decode(comment_input_interface.decryption_key.clone()).into_vec().unwrap()[0..32usize].try_into().unwrap();

    Ok(generate_comment_request_string::<
        32usize,
        8usize,
        32usize,
        16usize,
    >(encryption_input, comment_input_interface))?
}

#[allow(dead_code)]
fn repeatable_paste_request_body(
    paste_input_interface: &mut PasteInputInterface,
) -> Result<(String, String), Error> {
    const CIPHER_INITIAL_VECTOR_BASE64: &str = "xKODommtaYS112SVUe+4Rw==";
    const KEY_SALT_BASE64: &str = "NTZUAsbsAr4=";
    const KEY_PASSWORD_BASE64: &str = "a8g+i7A9M+V/oYwC7CjJ+MXJZik4nbVeh/9b2bG+5tU=";

    // Prepare starting values for encryption
    let mut encryption_input = encryption::EncryptionInput::<32usize, 8usize, 16usize>::new();
    encryption_input.cipher_initial_vector = base64::decode(CIPHER_INITIAL_VECTOR_BASE64)
        .context(Base64DecodeError {})?
        .try_into()
        .expect("Error converting Vec to byte array");

    encryption_input.key_salt = base64::decode(KEY_SALT_BASE64)
        .context(Base64DecodeError {})?
        .try_into()
        .expect("Error converting Vec to byte array");
    encryption_input.key_password = base64::decode(KEY_PASSWORD_BASE64)
        .context(Base64DecodeError {})?
        .try_into()
        .expect("Error converting Vec to byte array");

    Ok(generate_paste_request_string::<
        32usize,
        8usize,
        32usize,
        16usize,
    >(encryption_input, paste_input_interface))?
}

async fn response_from_request_string(
    request_body: &String,
    url: &url::Url,
) -> Result<String, Error> {
    let client = reqwest::Client::new();

    let request = client
        .post(url.clone())
        .header("Content-Type", "application/json")
        .header("X-Requested-With", "JSONHttpRequest")
        .body(request_body.clone());

    let response = request.send().await.context(HttpError {
        task_verb: String::from("sending request to the web API"),
    })?;
    let response_body = response.text().await.context(HttpError {
        task_verb: String::from("parsing body from the web response"),
    })?;

    return Ok(response_body);
}

// endregion: PRIVATE FUNCTIONS

// region: PUBLIC FUNCTIONS

pub async fn create_paste(
    paste_input_interface: &mut PasteInputInterface,
    debug: bool,
) -> Result<interface::PasteOutputInterface, Error> {
    let (request_string, decryption_key) = privatebin::paste_request_body(paste_input_interface)?;
    let response_string =
        privatebin::response_from_request_string(&request_string, &paste_input_interface.url)
            .await?;

    let paste_response: privatebin::ResponseData =
        serde_json::from_str(&response_string).context(JsonDeserializeError {
            given_data: format!("{:?}", &response_string),
        })?;

    if debug {
        println!("\nUser input: {:#?}\n", &paste_input_interface);
        println!("\nHTTP request: {}\n", &request_string);
        println!("\nDecryption key: {}\n", &decryption_key);
        println!("\nHTTP response: {:#?}\n", &response_string);
    }

    if paste_response.status == 0 {
        let paste_id = paste_response.id.clone().unwrap();
        let deletion_token = paste_response.deletetoken.clone().unwrap();
        let paste_output_interface = PasteOutputInterface {
            paste_id,
            decryption_key: decryption_key.clone(),
            deletion_token,
            url: Url::parse(
                format!(
                    "{}?{}#{}",
                    &paste_input_interface.url.as_str(),
                    &paste_response.id.as_ref().unwrap(),
                    decryption_key
                )
                .as_str(),
            )
            .unwrap(),
            deletion_url: Url::parse(
                format!(
                    "{}?pasteid={}&deletetoken={}",
                    &paste_input_interface.url.as_str(),
                    &paste_response.id.as_ref().unwrap(),
                    paste_response.deletetoken.unwrap()
                )
                .as_str(),
            )
            .unwrap(),
        };

        return Ok(paste_output_interface);
    }

    return Err(Error::WebApiError {
        task_name: "Paste creation".to_string(),
        message: paste_response.message.unwrap(),
    });
}

pub async fn create_comment(
    comment_input_interface: &mut CommentInputInterface,
    debug: bool,
) -> Result<interface::CommentOutputInterface, error::Error> {
    let (request_string, decryption_key) =
        privatebin::comment_request_body(comment_input_interface)?;
    let response_string =
        privatebin::response_from_request_string(&request_string, &comment_input_interface.url)
            .await?;

    let comment_response: privatebin::ResponseData = serde_json::from_str(&response_string)
        .context(JsonDeserializeError {
            given_data: format!("{:?}", &response_string),
        })?;

    if debug {
        println!("\nUser input: {:#?}\n", &comment_input_interface);
        println!("\nHTTP request: {}\n", &request_string);
        println!("\nDecryption key: {}\n", &decryption_key);
        println!("\nHTTP response: {:#?}\n", &response_string);
    }

    if comment_response.status == 0 {
        let comment_id = comment_response.id.clone().unwrap();
        let comment_output_interface = CommentOutputInterface {
            comment_id,
            url: Url::parse(
                format!(
                    "{}?{}#{}",
                    &comment_input_interface.url.as_str(),
                    &comment_response.id.as_ref().unwrap(),
                    decryption_key
                )
                .as_str(),
            )
            .unwrap(),
        };

        return Ok(comment_output_interface);
    }

    return Err(Error::WebApiError {
        task_name: "Comment creation".to_string(),
        message: comment_response.message.unwrap(),
    });
}

pub async fn replace_paste(
    paste_input_interface: &mut PasteInputInterface,
    debug: bool,
) -> Result<interface::PasteOutputInterface, error::Error> {
    todo!()
}

pub async fn replace_comment(
    comment_input_interface: &mut CommentInputInterface,
    debug: bool,
) -> Result<interface::CommentOutputInterface, error::Error> {
    todo!()
}

pub async fn delete_paste(
    delete_input_interface: &mut DeletionInputInterface,
    debug: bool,
) -> Result<Url, error::Error> {
    todo!()
}

// endregion: PUBLIC FUNCTIONS
