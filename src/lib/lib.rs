//┌────────────────────────────────────────────────────────────────────────────┐
//│ SDaRT - System Diagnostics and Repair Tool                                 │
//│ ==========================================                                 │
//│ File   : sdart-rs/src/lib/lib.rs                                           │
//│ License: Mozilla Public License 2.0                                        │
//│ URL    : https://gitlab.com/shivanandvp/sdart-rs                           │
//│ Authors:                                                                   │
//│     1. shivanandvp <shivanandvp@rebornos.org>                              │
//│     2.                                                                     │
//│ -----                                                                      │
//│ Description:                                                               │
//│                                                                            │
//│ The library crate's entry point. This crate will be named `sdart`          │
//│ by default based on the package name in `Cargo.toml`.                      │           
//│ -----                                                                      │
//│ Last Modified: Sun, 5th December 2021 6:24:12 PM                           │
//│ Modified By  : shivanandvp                                                 │
//│ -----                                                                      │
//│ 1. Copyright (c) 2021 shivanandvp <shivanandvp@rebornos.org>               │
//│ 2.                                                                         │
//└────────────────────────────────────────────────────────────────────────────┘



// region: API FACADE and IMPORTS

// Declares and re-exports the existence of inner modules in other files
pub mod ui;
pub mod paste;
pub mod chroot;
pub mod diagnostics;
pub mod repair;

// endregion: API FACADE and IMPORTS
