//┌────────────────────────────────────────────────────────────────────────────┐
//│ SDaRT - System Diagnostics and Repair Tool                                 │
//│ ==========================================                                 │
//│ File   : sdart-rs/src/lib/diagnostics/mod.rs                               │
//│ License: Mozilla Public License 2.0                                        │
//│ URL    : https://gitlab.com/shivanandvp/sdart-rs                           │
//│ Authors:                                                                   │
//│     1. shivanandvp <shivanandvp@rebornos.org>                              │
//│     2.                                                                     │
//│ -----                                                                      │
//│ Description:                                                               │
//│                                                                            │
//│                                                                            │
//│ -----                                                                      │
//│ Last Modified: Sat, 4th December 2021 5:31:28 PM                           │
//│ Modified By  :                                                             │
//│ -----                                                                      │
//│ 1. Copyright (c) 2021 shivanandvp <shivanandvp@rebornos.org>               │
//│ 2.                                                                         │
//└────────────────────────────────────────────────────────────────────────────┘

// region: API FACADE and IMPORTS

// Declares and re-exports the existence of inner modules in other files
pub mod logs;

// Re-exports internal items for use outside the module
pub use error::*;

// endregion: API FACADE and IMPORTS

mod error {
    // region: API FACADE and IMPORTS

    // Privately imports for use in this module
    use snafu::Snafu;

    // endregion: API FACADE and IMPORTS

    #[derive(Debug, Snafu)]
    #[snafu(visibility(pub))]
    pub enum Error {
        #[snafu(display("Unable to convert the data {} into YAML : {}", given_data, source))]
        YamlSerializeError {
            given_data: String,
            source: serde_yaml::Error,
        },

        #[snafu(display(
            "Unable to convert the YAML data {} into objects : {}",
            given_data,
            source
        ))]
        YamlDeserializeError {
            given_data: String,
            source: serde_yaml::Error,
        },

        #[snafu(display(
            "Unsupported or Invalid log source \"{}\". Please check the spelling or list of supported log sources.",
            log_source,
        ))]
        UnsupportedLogSourceError {
            log_source: String,
        },

        #[snafu(display(
            "Unable to read file at {}: {}",
            filepath.to_str().unwrap(),
            source,
        ))]
        LogReadError {
            filepath: std::path::PathBuf,
            source: std::io::Error,
        },
    }
}
