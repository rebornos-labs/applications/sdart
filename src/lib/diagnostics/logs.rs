//┌────────────────────────────────────────────────────────────────────────────┐
//│ SDaRT - System Diagnostics and Repair Tool                                 │
//│ ==========================================                                 │
//│ File   : sdart-rs/src/lib/diagnostics/logs.rs                              │
//│ License: Mozilla Public License 2.0                                        │
//│ URL    : https://gitlab.com/shivanandvp/sdart-rs                           │
//│ Authors:                                                                   │
//│     1. shivanandvp <shivanandvp@rebornos.org>                              │
//│     2.                                                                     │
//│ -----                                                                      │
//│ Description:                                                               │
//│                                                                            │
//│                                                                            │
//│ -----                                                                      │
//│ Last Modified: Sat, 4th December 2021 6:17:31 PM                           │
//│ Modified By  :                                                             │
//│ -----                                                                      │
//│ 1. Copyright (c) 2021 shivanandvp <shivanandvp@rebornos.org>               │
//│ 2.                                                                         │
//└────────────────────────────────────────────────────────────────────────────┘

// region: API FACADE and IMPORTS

// Re-export items for use outside the module
pub use configuration::*;

// endregion: API FACADE and IMPORTS

mod configuration {

    // region: API FACADE and IMPORTS

    // Re-export items for use outside the module
    pub use data_model::*;

    // endregion: API FACADE and IMPORTS

    mod data_model {
        // region: API FACADE and IMPORTS

        // Privately imports for use in this module
        use crate::diagnostics::*;
        use serde::{Deserialize, Serialize};
        use snafu::{OptionExt, ResultExt};
        use std::collections::HashMap;
        use std::path;

        // endregion: API FACADE and IMPORTS

        #[derive(Clone, Serialize, Deserialize)]
        pub struct LogList {
            version: String,
            logs: HashMap<String, LogSourceEntry>,
        }

        #[derive(Clone, Serialize, Deserialize)]
        pub struct LogSourceEntry {
            path: path::PathBuf,
        }

        impl LogList {
            pub fn new() -> Result<Self, Error> {
                LogList::from_yaml(std::include_str!("loglist.yaml"))
            }

            pub fn from_yaml<S: AsRef<str>>(yaml_text: S) -> Result<Self, Error> {
                return serde_yaml::from_str(yaml_text.as_ref()).context(YamlDeserializeError {
                    given_data: yaml_text.as_ref(),
                });
            }

            pub fn log_text<S: AsRef<str>>(&self, log_source: S) -> Result<String, Error> {
                let filepath = &self
                    .logs
                    .get(log_source.as_ref())
                    .context(UnsupportedLogSourceError {
                        log_source: log_source.as_ref().to_string(),
                    })?
                    .path;

                std::fs::read_to_string(filepath).context(LogReadError { filepath })
            }
        }
    }
}
