//┌────────────────────────────────────────────────────────────────────────────┐
//│ SDaRT - System Diagnostics and Repair Tool                                 │
//│ ==========================================                                 │
//│ File   : sdart-rs/src/sdart-paste.rs                                       │
//│ License: Mozilla Public License 2.0                                        │
//│ URL    : https://gitlab.com/shivanandvp/sdart-rs                           │
//│ Authors:                                                                   │
//│     1. shivanandvp <shivanandvp@rebornos.org>                              │
//│     2.                                                                     │
//│ -----                                                                      │
//│ Description:                                                               │
//│                                                                            │
//│                                                                            │
//│ -----                                                                      │
//│ Last Modified: Fri, 17th December 2021 8:32:17 AM                          │
//│ Modified By  :                                                             │
//│ -----                                                                      │
//│ 1. Copyright (c) 2021 shivanandvp <shivanandvp@rebornos.org>               │
//│ 2.                                                                         │
//└────────────────────────────────────────────────────────────────────────────┘

use clap::{crate_authors, crate_version, load_yaml, App};

mod commandline {
    use clap::{App, ArgMatches};
    use sdart::paste::{self, *};
    use std::path;
    use url::Url;

    struct GlobalArguments {
        pub json_flag: bool,
        pub plain_flag: bool,
        pub debug_flag: bool,
        pub qr_code_flag: bool,
        pub url: Url,
    }

    #[derive(Debug, Clone)]
    struct PasteSource<'a> {
        optional_filepath: Option<&'a str>,
    }

    impl<'a> PasteSource<'a> {
        pub fn new<'b: 'a>(optional_filepath: Option<&'b str>) -> Self {
            PasteSource { optional_filepath }
        }

        pub fn into_text(self) -> String {
            use std::{fs, io, io::Read};

            let mut paste_text = String::from("");
            if let Some(filepath) = self.optional_filepath {
                paste_text = fs::read_to_string(filepath).unwrap();
            } else {
                io::stdin().read_to_string(&mut paste_text).unwrap();
            }

            return paste_text;
        }
    }

    #[derive(Debug, Clone)]
    struct CommentSource<'a> {
        optional_text: Option<&'a str>,
    }

    impl<'a> CommentSource<'a> {
        pub fn new<'b: 'a>(optional_text: Option<&'b str>) -> Self {
            CommentSource { optional_text }
        }

        pub fn into_text(self) -> String {
            use std::{io, io::Read};

            let mut comment_text = String::from("");
            if let Some(text) = self.optional_text {
                comment_text = text.into();
            } else {
                io::stdin().read_to_string(&mut comment_text).unwrap();
            }

            return comment_text;
        }
    }

    fn paste_input_interface(matches: &ArgMatches) -> Result<paste::PasteInputInterface, Error> {
        Ok(paste::PasteInputInterface {
            paste_text: PasteSource::new(matches.value_of("filepath")).into_text(),
            expiry_duration: matches.value_of("expiry_duration").unwrap().into(),
            burn_after_reading: matches.is_present("burn_after_reading"),
            open_discussion: matches.is_present("open_discussion"),
            paste_display_format: matches
                .value_of("display_format")
                .unwrap()
                .to_string()
                .try_into()?,
            url: matches
                .value_of("url")
                .unwrap()
                .try_into()
                .unwrap_or(url::Url::parse("https://paste.rebornos.org").unwrap()),
            password: matches.value_of("password").unwrap().into(),
            compression_type: matches
                .value_of("compression_type")
                .unwrap()
                .to_string()
                .try_into()?,
            cipher_algorithm: matches
                .value_of("cipher_algorithm")
                .unwrap()
                .to_string()
                .try_into()?,
            cipher_mode: matches
                .value_of("cipher_mode")
                .unwrap()
                .to_string()
                .try_into()?,
            id: matches.value_of("expiry_duration").map(Into::into),
        })
    }

    fn comment_input_interface(
        matches: &ArgMatches,
    ) -> Result<paste::CommentInputInterface, Error> {
        Ok(paste::CommentInputInterface {
            comment_text: CommentSource::new(matches.value_of("text")).into_text(),
            url: matches
                .value_of("url")
                .unwrap()
                .try_into()
                .unwrap_or(url::Url::parse("https://paste.rebornos.org").unwrap()),
            password: matches.value_of("password").unwrap().into(),
            compression_type: matches
                .value_of("compression_type")
                .unwrap()
                .to_string()
                .try_into()?,
            cipher_algorithm: matches
                .value_of("cipher_algorithm")
                .unwrap()
                .to_string()
                .try_into()?,
            cipher_mode: matches
                .value_of("cipher_mode")
                .unwrap()
                .to_string()
                .try_into()?,
            paste_id: matches.value_of("paste_id").unwrap().into(),
            parent_id: matches
                .value_of("paste_id")
                .unwrap_or(matches.value_of("paste_id").unwrap())
                .into(),
            nickname: matches.value_of("nickname").unwrap().into(),
            decryption_key: matches.value_of("decryption_key").unwrap().into(),
        })
    }

    async fn create_paste(matches: &ArgMatches, global_arguments: GlobalArguments) -> Result<(), Error> {
        let paste_output_interface = paste::privatebin::create_paste(&mut paste_input_interface(matches)?, global_arguments.debug_flag).await?;            
            if !global_arguments.json_flag {
                use owo_colors::{OwoColorize, Stream::Stdout};
                println!("{}", "Paste creation successful...".if_supports_color(Stdout, |text| text.green()));
                println!("{}: {}", "Paste URL".if_supports_color(Stdout, |text| text.magenta()), paste_output_interface.url);
                println!("{}: {}", "Paste ID".if_supports_color(Stdout, |text| text.magenta()), paste_output_interface.paste_id);
                println!("{}: {}", "Decryption Key".if_supports_color(Stdout, |text| text.magenta()), paste_output_interface.decryption_key);
                println!("{}: {}", "Deletion URL".if_supports_color(Stdout, |text| text.magenta()), paste_output_interface.deletion_url);
                println!("{}: {}", "Deletion token".if_supports_color(Stdout, |text| text.magenta()), paste_output_interface.deletion_token);

                if global_arguments.qr_code_flag {
                    use std::process::Command;
                    Command::new("qrencode").arg("-t").arg("UTF8").arg::<String>(paste_output_interface.url.into()).status().expect("Error while running `qrencode`. Please check if the application is installed.");
                }
            } else {
                println!("{}", serde_json::to_string(&paste_output_interface).unwrap());
            }
        Ok(())
    }

    async fn create_comment(matches: &ArgMatches, global_arguments: GlobalArguments) -> Result<(), Error> {
        let comment_output_interface = paste::privatebin::create_comment(&mut comment_input_interface(matches)?, global_arguments.debug_flag).await?;            
            if !global_arguments.json_flag {
                println!("Comment creation successful...");
                println!("Comment URL: {}", comment_output_interface.url);
                println!("Comment ID: {}", comment_output_interface.comment_id);

                if global_arguments.qr_code_flag {
                    use std::process::Command;
                    Command::new("qrencode").arg("-t").arg("UTF8").arg::<String>(comment_output_interface.url.into()).status().expect("Error while running `qrencode`. Please check if the application is installed.");
                }
            } else {
                println!("{}", serde_json::to_string(&comment_output_interface).unwrap());
            }
        Ok(())
    }

    pub async fn process_options<'a>(matches: &ArgMatches) -> Result<(), Error> {
        let global_arguments = GlobalArguments {
            json_flag: matches.is_present("json"),
            plain_flag: matches.is_present("plain"),
            debug_flag: matches.is_present("debug"),
            qr_code_flag: matches.is_present("qr_code"),
            url: matches
            .value_of("url")
            .unwrap()
            .try_into()
            .unwrap_or(url::Url::parse("https://paste.rebornos.org").unwrap()),
        };
        
        if let None = matches.subcommand_name() {
            create_paste(matches, global_arguments).await?;
        } else if let Some(matches) = matches.subcommand_matches("create") {
            if let Some(matches) = matches.subcommand_matches("paste") {
                create_paste(matches, global_arguments).await?;
            } else if let Some(matches) = matches.subcommand_matches("comment") {
                create_comment(matches, global_arguments).await?;
            }
        } else {
            eprintln!("Error: Subcommand not supported");
            std::process::exit(1);
        }
        Ok(())
    }


    // pub async fn handle_options(options: Options) -> Result<(), sdart::paste::Error> {
    //     use sdart::paste::*;
    //     match options {
    //         Options {
    //             json,
    //             debug,
    //             action_type:
    //                 ActionType::Create {
    //                     create_type:
    //                         CreateType::Paste {
    //                             filepath,
    //                             password,
    //                             url,
    //                             expiry_duration,
    //                             burn_after_reading,
    //                             open_discussion,
    //                             paste_display_format,
    //                             compression_type,
    //                             cipher_algorithm,
    //                             cipher_mode,
    //                             qr_code,
    //                         },
    //                 },
    //         } => {
    //             let paste_text = text_from_filepath_or_stdin(filepath)?;
    //             let paste_output_interface = create_paste(&mut PasteInputInterface {
    //                 paste_text,
    //                 password,
    //                 url,
    //                 id: None,
    //                 expiry_duration,
    //                 burn_after_reading,
    //                 open_discussion,
    //                 paste_display_format: paste_display_format.try_into()?,
    //                 compression_type: compression_type.try_into()?,
    //                 cipher_algorithm: cipher_algorithm.try_into()?,
    //                 cipher_mode: cipher_mode.try_into()?,
    //             }, debug)
    //             .await?;
    //             if !json {
    //                 use owo_colors::{OwoColorize, Stream::Stdout};
    //                 println!("{}", "Paste creation successful...".if_supports_color(Stdout, |text| text.bright_blue()));
    //                 println!("{}: {}", "Paste URL".if_supports_color(Stdout, |text| text.magenta()), paste_output_interface.url);
    //                 println!("{}: {}", "Deletion URL".if_supports_color(Stdout, |text| text.magenta()), paste_output_interface.deletion_url);
    //                 println!("{}: {}", "Paste ID".if_supports_color(Stdout, |text| text.magenta()), paste_output_interface.paste_id);
    //                 println!("{}: {}", "Deletion token".if_supports_color(Stdout, |text| text.magenta()), paste_output_interface.deletion_token);

    //                 if qr_code {
    //                     use std::process::Command;
    //                     Command::new("qrencode").arg("-t").arg("UTF8").arg::<String>(paste_output_interface.url.into()).status().expect("Error while running `qrencode`. Please check if the application is installed.");
    //                 }
    //             } else {
    //                 println!("{}", serde_json::to_string(&paste_output_interface).unwrap());
    //             }
    //         }
    //         Options {
    //             json,
    //             debug,
    //             action_type:
    //                 ActionType::Create {
    //                     create_type:
    //                         CreateType::Comment {
    //                             comment_text,
    //                             password,
    //                             url,
    //                             paste_id,
    //                             parent_id,
    //                             compression_type,
    //                             cipher_algorithm,
    //                             cipher_mode,
    //                         },
    //                 },
    //         } => {
    //             let comment_output_interface = create_comment(&mut CommentInputInterface {
    //                 comment_text,
    //                 password,
    //                 url,
    //                 paste_id: paste_id.clone(),
    //                 parent_id: parent_id.unwrap_or(paste_id),
    //                 compression_type: compression_type.try_into()?,
    //                 cipher_algorithm: cipher_algorithm.try_into()?,
    //                 cipher_mode: cipher_mode.try_into()?,
    //             }, debug)
    //             .await?;
    //             if !json {
    //                 println!("Comment creation successful...");
    //                 println!("Comment URL: {}", comment_output_interface.url);
    //                 println!("Comment ID: {}", comment_output_interface.comment_id);
    //             } else {
    //                 println!("{}", serde_json::to_string(&comment_output_interface).unwrap());
    //             }
    //         }
    //         Options {
    //             json,
    //             debug,
    //             action_type:
    //                 ActionType::Replace {
    //                     replace_type:
    //                         ReplaceType::Paste {
    //                             filepath,
    //                             password,
    //                             url,
    //                             paste_id,
    //                             expiry_duration,
    //                             burn_after_reading,
    //                             open_discussion,
    //                             paste_display_format,
    //                             compression_type,
    //                             cipher_algorithm,
    //                             cipher_mode,
    //                         },
    //                 },
    //         } => {
    //             let paste_text = text_from_filepath_or_stdin(filepath)?;
    //             let paste_output_interface = replace_paste(&mut PasteInputInterface {
    //                 paste_text,
    //                 password,
    //                 url,
    //                 id: Some(paste_id),
    //                 expiry_duration,
    //                 burn_after_reading,
    //                 open_discussion,
    //                 paste_display_format: paste_display_format.try_into()?,
    //                 compression_type: compression_type.try_into()?,
    //                 cipher_algorithm: cipher_algorithm.try_into()?,
    //                 cipher_mode: cipher_mode.try_into()?,
    //             }, debug)
    //             .await?;
    //             if !json {
    //                 println!("Paste replacement successful...");
    //                 println!("Paste URL: {}", paste_output_interface.url);
    //                 println!("Deletion URL: {}", paste_output_interface.deletion_url);
    //                 println!("Paste ID: {}", paste_output_interface.paste_id);
    //                 println!("Deletion token: {}", paste_output_interface.deletion_token);
    //             } else {
    //                 println!("{}", serde_json::to_string(&paste_output_interface).unwrap());
    //             }
    //         }
    //         Options {
    //             json,
    //             debug,
    //             action_type:
    //                 ActionType::Replace {
    //                     replace_type:
    //                         ReplaceType::Comment {
    //                             comment_text,
    //                             password,
    //                             url,
    //                             comment_id,
    //                             compression_type,
    //                             cipher_algorithm,
    //                             cipher_mode,
    //                         },
    //                 },
    //         } => {
    //             let comment_output_interface = replace_comment(&mut CommentInputInterface {
    //                 comment_text,
    //                 password,
    //                 url,
    //                 paste_id: comment_id.clone(),
    //                 parent_id: comment_id.clone(),
    //                 compression_type: compression_type.try_into()?,
    //                 cipher_algorithm: cipher_algorithm.try_into()?,
    //                 cipher_mode: cipher_mode.try_into()?,
    //             }, debug)
    //             .await?;
    //             if !json {
    //                 println!("Comment replacement successful...");
    //                 println!("Comment URL: {}", comment_output_interface.url);
    //                 println!("Comment ID: {}", comment_output_interface.comment_id);
    //             } else {
    //                 println!("{}", serde_json::to_string(&comment_output_interface).unwrap());
    //             }
    //         }
    //         Options {
    //             json,
    //             debug,
    //             action_type:
    //                 ActionType::Delete {
    //                     url,
    //                     paste_id,
    //                     delete_token,
    //                 },
    //         } => {
    //             delete_paste(&mut DeletionInputInterface {
    //                 paste_id,
    //                 delete_token,
    //             }, debug)
    //             .await?;
    //             if !json {
    //                 println!("Paste deletion successful...");
    //             } else {
    //                 println!("{}", serde_json::to_string(&DeletionOutputInterface{}).unwrap());
    //             }
    //         }
    //     };

    //     Ok(())
    // }
}

#[tokio::main]
async fn main() {
    let yaml = load_yaml!("sdart_paste_cli.yaml");
    let mut app = App::from(yaml)
        .author(crate_authors!("\n"))
        .version(crate_version!());
    let matches = app.get_matches();
    if let Err(error) = commandline::process_options(&matches).await {
        eprintln!("Error: {}", error);
        std::process::exit(1);
    }
}
