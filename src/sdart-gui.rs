//┌────────────────────────────────────────────────────────────────────────────┐
//│ SDaRT - System Diagnostics and Repair Tool                                 │
//│ ==========================================                                 │
//│ File   : sdart-rs/src/sdart-gui.rs                                         │
//│ License: Mozilla Public License 2.0                                        │
//│ URL    : https://gitlab.com/shivanandvp/sdart-rs                           │
//│ Authors:                                                                   │
//│     1. shivanandvp <shivanandvp@rebornos.org>                              │
//│     2.                                                                     │
//│ -----                                                                      │
//│ Last Modified: Sun, 5th December 2021 6:25:03 PM                           │
//│ Modified By  : shivanandvp                                                 │
//│ -----                                                                      │
//│ 1. Copyright (c) 2021 shivanandvp <shivanandvp@rebornos.org>               │
//│ 2.                                                                         │
//└────────────────────────────────────────────────────────────────────────────┘

use sdart::ui;

fn main() {
    ui::gtk4::load();
}
