#! /usr/bin/env sh

#┌─────────────────────────────────────────────────────────────────────────────┐
#│ SDaRT - System Diagnostics and Repair Tool                                  │
#│ ==========================================                                  │
#│ File   : sdart-rs/packaging/build-package.sh                                │
#│ License: Mozilla Public License 2.0                                         │
#│ URL    : https://gitlab.com/shivanandvp/sdart-rs                            │
#│ Authors:                                                                    │
#│     1. shivanandvp <shivanandvp@rebornos.org>                               │
#│     2.                                                                      │
#│ -----                                                                       │
#│ Last Modified: Mon, 6th December 2021 6:56:51 PM                            │
#│ Modified By  : shivanandvp                                                  │
#│ -----                                                                       │
#│ 1. Copyright (c) 2021 shivanandvp <shivanandvp@rebornos.org>                │
#│ 2.                                                                          │
#└─────────────────────────────────────────────────────────────────────────────┘

# Parse arguments
LONG=source:,target:,clean,install,help
SHORT=s:,t:,b:,c,i,h
OPTS=$(getopt --alternative --name build-package --options $SHORT --longoptions $LONG -- "$@") 
eval set -- "$OPTS"

# Set default options
TARGET="arch"
SOURCE="local"
BRANCH=""
CLEAN="n"
INSTALL="n"

display_help()
{
    echo "Usage:                                                                        "
    echo "    build-package -h|--help                                                   "
    echo "    build-package [-s|--source <SOURCE>] [-t|--target <TARGET>]               "
    echo "                            [-b|--branch <BRANCH>] [-c|--clean] [-i|--install]"
    echo "Arguments:                                                                    "
    echo "    <SOURCE>:                                                                 "
    echo "        local     Use the local source code                                   "
    echo "        git       Pull sources from the git repository                        "
    echo "    <TARGET>:                                                                 "
    echo "        arch      Build for Arch Linux                                        "
    echo "    <BRANCH>:     Name of the git branch to use                               "
}

build_package()
{
    TARGET="$1"
    SOURCE="$2"
    BRANCH="$3"
    CLEAN="$4"
    INSTALL="$5"

    if [ "$TARGET" == "arch" ]; then
        build_package_arch "$SOURCE" "$BRANCH" "$CLEAN" "$INSTALL"
    else
        echo "Target $1 is not supported yet. Please contact the developer." 1>&2
        exit 3
    fi
}

build_package_arch()
{
    SOURCE="$1"
    BRANCH="$2"
    CLEAN="$3"
    INSTALL="$4"
    PARENTDIRECTORY=$(dirname $(readlink -f $0)) # Resolve any symlinks and then go to the parent directory
    
    BUILD_DIRECTORY="/tmp/makepkg"
    SOURCE_PATH="$PARENTDIRECTORY/../sdart-rs"
    DESTINATION_PATH="$BUILD_DIRECTORY/sdart/src"

    SOURCE_PATH="$(dirname "$SOURCE_PATH")" # Resolve the source path

    if [ "$SOURCE" == "local" ]; then
        PKGBUILD_POSTFIX="local"
        mkdir -p "$DESTINATION_PATH" # Create the directory used by `makepkg` for sources
        CURRENT_BRANCH="$(git branch --show-current)"
        if [ -n "$STRING" ]; then
          git checkout "$BRANCH"
        fi 
        cp -r "$SOURCE_PATH" "$DESTINATION_PATH" # Copy the sources from local directory
        cp -r "$PARENTDIRECTORY/archlinux/sdart.desktop" "$DESTINATION_PATH" # Copy the desktop file from local directory
        git checkout "$CURRENT_BRANCH"
    else
        PKGBUILD_POSTFIX="git"
    fi

    if [ "$CLEAN" == "y" ]; then
        CLEAN_ARGUMENT="--cleanbuild"
    else
        CLEAN_ARGUMENT=""
    fi 

    if [ "$INSTALL" == "y" ]; then
        INSTALL_ARGUMENT="--install"
    else
        INSTALL_ARGUMENT=""
    fi 

    ( # Create a subshell to prevent directory changes outside
      cd "$PARENTDIRECTORY/archlinux" && \
      BUILDDIR="$BUILD_DIRECTORY" \
      MAKEFLAGS="-j$(nproc)" \
      makepkg -p PKGBUILD-"$PKGBUILD_POSTFIX" \
          "$CLEAN_ARGUMENT" \
          --noextract \
          --force \
          "$INSTALL_ARGUMENT" \
          --syncdeps \
          --sign
    )
}

while :
do
  case "$1" in
    -s | --source )
      SOURCE="$2"
      SOURCE="$SOURCE" | awk '{print tolower($0)}'
      shift 2
      ;;
    -t | --target )
      TARGET="$2"
      TARGET="$TARGET" | awk '{print tolower($0)}'
      shift 2
      ;;
    -c | --clean )
      CLEAN="y"
      shift
      ;;
    -i | --install )
      INSTALL="y"
      shift
      ;;
    -h | --help)
      display_help
      exit 0
      ;;
    --)
      shift;
      break
      ;;
    *)
      echo "Unexpected option: $1" 1>&2
      display_help
      exit 2
      ;;
  esac
done

build_package "$TARGET" "$SOURCE" "$BRANCH" "$CLEAN" "$INSTALL" 



