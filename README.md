# SDaRT - System Diagnostics and Repair Tool 

*Authors: shivanandvp@rebornos.org*

## Documentation

The API documentation for each binary is as below:

1. [sdart](https://shivanandvp.gitlab.io/sdart-rs/sdart/index.html)
2. [sdart-gui](https://shivanandvp.gitlab.io/sdart_rs/sdart-gui/index.html)
3. [sdart-chroot](https://shivanandvp.gitlab.io/sdart-rs/sdart_chroot/index.html)
4. [sdart-paste](https://shivanandvp.gitlab.io/sdart-rs/sdart_paste/index.html)

## License

This project is licensed under the [Mozilla Public License (MPL-2.0)](https://www.mozilla.org/en-US/MPL/2.0/)

## Status

This project is in the *Pre-Alpha* stage. Use at your own risk.

### ToDo List

1. Retrieve logs from installed Arch Linux systems (needs chrooting), also includes the current installed system
2. The ability to upload individual log files to a paste service (checkboxes)
    1. Cnchi
    2. Systemd
    3. LightDM
    4. GDM
    5. SDDM
    6. Nvidia
    7. Xorg
    8. AMD
    9. dmesg
3. The ability to select multiple logs and export as a compressed archive?? (Do we need it?)
4. The ablity to get system properties (checkboxes)
    1. Partition layout
    2. Network connections
    3. CPU information
    4. GPU information
5. the ability to redact personally identifiable information, automatically, and manually per file

